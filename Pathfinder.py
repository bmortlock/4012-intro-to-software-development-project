grid = [[1,2,0,0,0,0,1,0,0,0,1],
        [1,0,1,0,1,1,1,0,1,1,1],
        [1,0,1,0,1,0,0,0,0,0,1],
        [1,1,1,0,1,1,1,1,1,0,1],
        [1,0,1,0,0,0,0,0,1,0,1],
        [1,0,1,1,1,0,1,0,1,0,1],
        [1,0,1,0,0,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,1,1],
        [1,0,0,0,1,0,1,0,0,0,1]]

#1's indicate wall, 0's indicate path and 2 is destination
def search(x, y):

    #if number in grid is 2 output this
    if grid[x][y] == 2:

        print( 'Destination at %d,%d' % (x, y))

        return True
    #if number in grid is 1 output this
    elif grid[x][y] == 1:

        print ('Wall at %d,%d' % (x, y))

        return False

    elif grid[x][y] == 3:
        #if number in grid is 0 output this
        print ( 'Visited at %d,%d' % (x, y))

        return False

     

    print( 'Visiting %d,%d' % (x, y))

 

    # mark as visited if grid is 0

    grid[x][y] = 3

 

    # explore neighboring sections clockwise starting by the one on the right

    if ((x < len(grid)-1 and search(x+1, y))

        or (y > 0 and search(x, y-1))

        or (x > 0 and search(x-1, y))

        or (y < len(grid)-1 and search(x, y+1))):

        return True

    return False

#sets starting postion
search(8, 9)
