
//set node coordinates
var v0 = [350,350];
var v1 = [300,350];
var v2 = [300,300];
var v3 = [300,250];
var v4 = [250,250];
var v5 = [250,300];
var v6 = [250,350];
var v7 = [200,300];
var v8 = [200,350];
var v9 = [150,350];
var v10 = [150,300];
var v11 = [150,250];
var v12 = [200,250];
var v13 = [200,200];
var v14 = [200,150];
var v15 = [250,150];
var v16 = [150,150];
var v17 = [150,200];




var V =[v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17]
//create links between node
var E = [ [0,1],
		  [1,2],
		  [2,3],
	      [3,4],
		  [4,5],
          [5,6],
          [5,7],
          [7,8],
          [8,9],
          [9,10],
          [10,11],
          [4,12],
          [12,13],
          [13,14],
          [14,15],
          [14,16],
          [16,17]];

				 	
// set up some edge coordinate variables
var x1, x2, y1, y2 = 0;

function setup() {
  createCanvas(400, 400);
  background(220);
  var start = 0
  var destination = 16
  // Set text characteristics
  textSize(16);
  textAlign(CENTER, CENTER);
  // plot nodes first
  for (var i=0; i<V.length; i++) {
    // colour the nodes
    fill(0);x
    ellipse(V[i][0], V[i][1], 30, 30);
    // label the nodes
    fill(0);
    text("v"+str(i), V[i][0]-15, V[i][1]-24);
  }
  //polted edges
  for (var i=0; i<E.length; i++) {
    x1 = V[ E[i][0] ][0];
    y1 = V[ E[i][0] ][1];
    x2 = V[ E[i][1] ][0];
    y2 = V[ E[i][1] ][1];
    line(x1, y1, x2, y2);
    
    
  }
  //colour start and destination node
  for (var i=16; i<V.length; i++)
  fill(0,0,255);
  ellipse(350, 350, 30, 30); 
for (var i=0; i<V.length; i++)
  fill(255,0,0);
  ellipse(150, 150, 30, 30);
}

